import React from 'react';
import { Redirect, Route, withRouter } from 'react-router-dom';// 3.1
import AuthService from "./services/auth.service";

const PrivateRoute = ({ component: Component, ...rest }) => {
    const isLoggedIn = AuthService.getCurrentUser();

    return (
        <Route
            {...rest}
            render={props =>
                isLoggedIn ? (
                    <Component {...props} />
                ) : (
                        <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
                    )
            }
        />
    )
}
export default PrivateRoute;