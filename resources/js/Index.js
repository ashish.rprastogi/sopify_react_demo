import React, { useState, Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import Main from './Router';
// import "bootstrap/dist/css/bootstrap.min.css";

function Index() {
    return (
        <BrowserRouter>
            <Route render={()=><Main/>} exact />
        </BrowserRouter>
    );
}

export default Index;

if (document.getElementById('index')) {
    ReactDOM.render(<Index />, document.getElementById('index'));
}
