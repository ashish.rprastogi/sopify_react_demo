import React from "react";
import AuthService from "../services/auth.service";
import { Redirect, Route } from 'react-router-dom';
import AccountMenu from "./AccountMenu.js";

const Profile = () => {
    const currentUser = AuthService.getCurrentUser();
    return (
        <>
            <section className="setting_outer">
                <div className="container">
                    <div className="account_outer">
                        <h2>Account Settings</h2>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 col-md-4">
                            <div className="grey_outer8">
                                <AccountMenu />
                            </div>
                        </div>
                        <div className="col-lg-8 col-md-8">
                            <div className="right_cont56">
                                <div className="profile"><h4>Profile Image</h4></div>
                                <div className="middle_part">
                                    <div className="prifle_img">
                                        <img src="/images/user_pick.png" />
                                    </div>
                                    <div className="link8"><a href="#">Click Here Change Photo</a></div>
                                    <div className="up_btn"><a className="btn btn-info" role="button" href="#">Update</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default Profile;
