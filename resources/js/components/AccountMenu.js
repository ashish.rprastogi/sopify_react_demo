import React, { useState } from "react";
import { Redirect, Link, Route, useLocation } from 'react-router-dom';

function AccountMenu(props){
    const location = useLocation();
    return (
        <>
            <ul className="profile_outr57">
                <li><h3>Profile Management</h3></li>
                <li className={(location.pathname === '/profile')?"active9":''}>
                    <Link to={"/profile"} className="navbar-brand">
                        Profile
                    </Link>
                </li>
                <li className={(location.pathname === '/change_password') ? "active9" : ''}>
                    <Link to={"/change_password"} className="navbar-brand">
                        Change Password
                    </Link>
                </li>
            </ul>
            <ul className="profile_outr57">
                <li><h3>Account Management</h3></li>
                <li><a href="#">Logo</a></li>
                <li><a href="#">Manage Plan</a></li>
                <li><a href="#">Payment Method</a></li>
            </ul>
            <ul className="profile_outr57">
                <li><h3>User Management</h3></li>
                <li><a href="#">Add New User</a></li>
                <li><a href="#">Manage Existing User</a></li>
            </ul>
        </>
    );
};

export default AccountMenu;
