import React, { useState, useRef } from "react";
import { Link } from 'react-router-dom';

import AuthService from "../services/auth.service";
import "/css/style.css";

const Login = (props) => {
    const form = useRef();
    const checkBtn = useRef();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");

    const onChangeUsername = (e) => {
        const username = e.target.value;
        setUsername(username);
    };

    const onChangePassword = (e) => {
        const password = e.target.value;
        setPassword(password);
    };

    const handleLogin = (e) => {
        e.preventDefault();

        setMessage("");
        setLoading(true);


        AuthService.login(username, password).then(
            () => {
                props.history.push("/profile");
                window.location.reload();
            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();

                setLoading(false);
                setMessage(resMessage);
            }
        );
    };

    return (
        <section className="login_outer">
            <div className="container">
                <div className="row">
                    <div className="col-ld-8 col-md-8 col-sm-6">
                        <div className="login_img">
                            <img src="/images/login.png" />
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-4 col-sm-6">
                        <div className="login_right">
                            <div className="input_outer5 login_inner">
                                {message && (
                                    <div className="form-group">
                                        <div className="alert alert-danger" role="alert">
                                            {message}
                                        </div>
                                    </div>
                                )}
                                <form className="custom_form" onSubmit={handleLogin} ref={form}>
                                    <div className="form-group">
                                        <label htmlFor="name">Email</label>
                                        <input type="text" name="email" className="form-control" placeholder="Enter" value={username} onChange={onChangeUsername} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="pwd">Confirm Password:</label>
                                        <input type="password" className="form-control" placeholder="Enter" name="password" id="pwd" value={password} onChange={onChangePassword} />
                                        <div className="custom_pass9"><i className="fa fa-eye-slash" aria-hidden="true"></i></div>
                                    </div>
                                    <div className="forget_outer">
                                        <a href="#">Forgot Password?</a>
                                    </div>
                                    <div className="register5 login_btn">
                                        <button type="submit" className="btn btn-primary" disabled={loading}>
                                            {loading && (
                                                <span className="spinner-border spinner-border-sm"></span>
                                            )} &nbsp; Log In
                                        </button>
                                    </div>
                                </form>
                                <div className="member_outer56">
                                    <ul>
                                        <li>Not a Member</li>
                                        <li>
                                            <Link to={"/register"} className="nav-link">
                                                Sign Up
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Login;