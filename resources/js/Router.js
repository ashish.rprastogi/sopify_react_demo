import React, { useState, useEffect } from 'react';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

import AuthService from "./services/auth.service";

import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Profile from "./components/Profile";
import ChangePassword from "./components/ChangePassword";
import PrivateRoute from './PrivateRoute';

const Main = () => {
    const [currentUser, setCurrentUser] = useState(undefined);

    useEffect(() => {
        const user = AuthService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
        }
    }, []);

    const logOut = () => {
        AuthService.logout();
    };

    return (
        <>
        <header>
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light">
                    <Link to={"/"} className="navbar-brand">
                        <img src="/images/logo.png" />
                    </Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Product</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Use Cases</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Pricing</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">About</a>
                            </li>
                        </ul>
                            {currentUser ? (
                                <>
                                    <ul className="dsh_nav navbar-nav ml-auto">
                                        <li className="upgrade"><a className="btn btn-info" role="button" href="#">Upgrade</a></li>
                                        <li className="nav-item dropdown ">
                                            <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Account
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <Link to={"/profile"} className="dropdown-item">
                                                    Account Settings
                                                </Link>
                                                <a className="dropdown-item" href="/login" onClick={logOut}>Sign Out</a>
                                            </div>
                                        </li>
                                        <li className="nav-item cm_outer5 dh_user">
                                            <a className="nav-link disabled" href="#"><img src="/images/user_pick.png" /></a>
                                        </li>
                                    </ul>
                                </>
                            ) : (
                                <>
                                    <ul className="right_menu dashboard_menu">
                                        <li><a className="btn btn-info" role="button" href="#">Free Trial</a></li>
                                        <li>
                                            <Link to={"/login"} className="nav-link">
                                                Login
                                            </Link>
                                        </li>
                                    </ul>
                                </>
                            )}
                    </div>
                </nav>
            </div>
        </header>
        <div>
            <Switch>
                <Route exact path={["/", "/home"]} component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <PrivateRoute exact path="/profile" component={Profile} />
                <PrivateRoute exact path="/change_password" component={ChangePassword} />
            </Switch>
        </div>

        </>
    );
};

export default Main;
